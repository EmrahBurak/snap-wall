//query  resolvers
const Query = require('./queries/Query');
const Snap = require('./queries/Snap');
const User = require('./queries/User');

//mutation resolvers
const Mutation = require('./mutation/index');

// Subscription 
const Subscription = require('./subscriptions/index');
module.exports = {
    Query,
    Snap,
    User,
    Mutation,
    Subscription
}
