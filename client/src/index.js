import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import reportWebVitals from './reportWebVitals';
import { ApolloClient,
	 InMemoryCache,
	 createHttpLink,
	 ApolloLink,
	 ApolloProvider } from '@apollo/client';
//import { getMainDefinition } from '@apollo/client/utilities';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { setContext } from '@apollo/client/link/context';
import { WebSocketLink } from '@apollo/client/link/ws';

//const httpLink = createHttpLink({
//  uri: 'http://localhost:4001/graphql',
//});

const authLink = setContext((_,{headers}) => {
    const token  = localStorage.getItem('token');
    return {
	headers:{
	    ...headers,
	    authorization: token ? `Bearer ${token}` : "",
	}
    }
});



//const authLink = new ApolloLink((operation, forward) => {
//  // add the authorization to the headers
//  operation.setContext(({ headers = {} }) => ({
//    headers: {
//      ...headers,
//      authorization: localStorage.getItem('token') || null,
//    }
//  }));

//  return forward(operation);
//});

const wsLink = new WebSocketLink(
	new SubscriptionClient(process.env.REACT_APP_SUBSCRIPTION_URI, {
		reconnect: true,
	}),
);

const httpLink = authLink.concat(
	createHttpLink({
		uri: process.env.REACT_APP_HTTP_URI,
	})
);

const hasSubscriptionOperation = ({ query: { definitions } }) => {
	return definitions.some(({ kind, operation }) => kind === 'OperationDefinition' && operation === 'subscription');
};

const link = ApolloLink.split(
	hasSubscriptionOperation,
	wsLink,
	httpLink
);

const client = new ApolloClient({
	link,
	cache: new InMemoryCache(),
});





ReactDOM.render(
  <React.StrictMode>

   <ApolloProvider client={client}>
	<App />

   </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
