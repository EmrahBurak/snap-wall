import React,{ useState} from 'react';
import { useMutation } from '@apollo/client';
import { withRouter } from 'react-router-dom';
import Error from '../Error';
import {
    SIGNIN_USER
}
from '../../queries';



const initialState = {
	username:'',
    	password:'',
}

const Mutation = (props) => {
	const [signIn, { loading, error }] = useMutation(SIGNIN_USER);
    
    const [state, setState] = useState({
	...initialState
    	});


    const {
	username,
	password
    } = state;


    const OnChange = (e)=>{
    	setState(Object.assign({},state,{[e.target.name]:e.target.value}))
    };

    const FormValidate = () => {

	return (!username || !password);
    };

    const ResetState = () => {
	setState({
	    ...initialState
	})
    }




    return (
            <form className="user-form"
	onSubmit = {
	    e => {
		e.preventDefault();
		signIn({
		    variables: {
			username,
			password
		    }
		}).then(async({
		    data
		}) => {
		    console.log(data);
		    localStorage.setItem('token',data.signIn.token);
		    await props.refetch();
		    await ResetState();
		    props.history.push('/');
		}).catch(e => console.log(e.message));
	    }
	}
	    >
                <label>
            <input name="username" type="text"
	onChange={OnChange}
	placeholder = "username"
	value={
	    username
	}
	/>
                </label>
                <label>
            <input name="password" type="password"
	onChange={OnChange}
	placeholder="password"
	value={
	    password
	}
	    />
                </label>
                <label>
            <button disabled={loading || FormValidate()}>Login</button>
                </label>

	     { loading &&  <div>loading...</div>}
	     { error &&  <Error error={
		 error
	     }
	     />}

            </form>
    );
};


class Login extends React.Component{

    render(){

	return(
	     <div>
		<Mutation {...this.props}/>
            </div>
	);
    }
}

export default withRouter(Login);
