import React,{
    useState
} from 'react';
import { useMutation } from '@apollo/client';
import Error from '../Error';
import { withRouter } from 'react-router-dom';
import {
    CREATE_USER
}
from '../../queries';


const initialState = {
	username:'',
    	password:'',
    	passwordConfirm: ''

}


const Mutation = (props) => {
	const [createUser, { loading, error }] = useMutation(CREATE_USER);
    

    const [state, setState] = useState({
	...initialState
    	
    	});

    const {
	username,
	password,
	passwordConfirm
    } = state;


    const OnChange = (e)=>{
    	setState(Object.assign({},state,{[e.target.name]:e.target.value}))
    };

    const FormValidate = () => {

	const isInvalid = (!state.username || !state.password || !state.passwordConfirm || state.password !== state.passwordConfirm);

	return isInvalid
    };



    const ResetState = () => {
	setState({
	    ...initialState
	})
    }


    return ( <
	     div >
	     <
	     form className = "user-form"
	     onSubmit = {
		 e => {
		     e.preventDefault();
		     createUser({
		                 variables: {
				     username,
				     password
				 }
		     }).then(async({data}) => {
			 console.log(data);
			 localStorage.setItem('token',data.createUser.token);
			 await props.refetch();
			 await ResetState();
			 props.history.push('/');
		     })
			 .catch(e => console.log(e.message));

		 }
	     }>
                <label>
                <input name="username" onChange={
		    OnChange
		}
	     type="text" placeholder="username" value={username} />
                </label>
                <label>
                <input name="password" onChange={
		    OnChange
		}
	     type="password" placeholder="password" value={
		 password
	     } />
                </label>
                <label>
                <input name="passwordConfirm"  onChange={
		    OnChange
		}
	     type="password" placeholder="confirm password" value={
		 passwordConfirm
	     } />
                </label>
                <label>
             <button disabled={loading || FormValidate()}>Join</button>
                </label>
	     { loading &&  <div>loading...</div>}
	     { error &&  <Error error={
		 error
	     }
	     />}
            </form>
	    </div>
    )


}



class Join extends React.Component{
    

    render(){

	return(
	<div>
		<Mutation {...this.props}/>
    
        </div>
	)

    }

}

export default withRouter(Join);
