
import React from 'react';
import NewSnapForm from './NewSnapForm';
import SnapList from './SnapList';
import JoinedUs from './JoinedUs';



class Home extends React.Component {


    
    render() {
	const {
	    session
	} = this.props;

	return (
		< div >
		<div className="description">
		<p className="sub_header__desc">simple snap app with <span>react</span>.</p>
		</div>
		
		<NewSnapForm session={
		    session
		}/>
		<JoinedUs/>
		<SnapList session={ session }/>
		
		</div>

	)
    }
}

export default Home;
