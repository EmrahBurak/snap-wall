import React,{Component,useState, useEffect} from 'react';
import {  useMutation } from '@apollo/client';
import { GET_SNAPS,ADD_SNAP } from '../../../queries';


const Mutation = ({session}) => {
    const [createSnap, {
	error
    }] = useMutation(ADD_SNAP);
    const [state, setState] = useState({
	text: '',
	user_id: ''
    });

    const {
	text,
	user_id
    } = state;


    const FormValidate = ()=>{
	const {text } = state;
	return !text;
    }

    const OnChange = (e)=>{
    	setState(Object.assign({},state,{[e.target.name]:e.target.value}))
    };


    useEffect(() => {
	if (session && session.activeUser) {
	    setState(state=>({...state,
			      user_id: session.activeUser.id,

	    }));
	};
    }, [session, text]);

	

    if (error) return <p > error < /p>;
    return (
	<div>
	    < form onSubmit = {
		e => {
		    e.preventDefault();
		    if(!FormValidate()){
			setState({
			    text: '',
			});
			createSnap({
		                variables: {
				    text,
				    user_id
				},
			    //refetchQueries:[{query: GET_SNAPS}]
			    update: (cache, {data: {createSnap}}) => {
				const {
				    snaps
				} =  cache.readQuery({
				    query: GET_SNAPS
				});

				 cache.writeQuery({
				    query: GET_SNAPS,
				    data: {
					snaps:[createSnap,...snaps]
				    }
				});
			    },
			    optimisticResponse: {
				__typename: "Mutation",
				createSnap: {
				    __typename: "Snap",
				    id: Math.round(Math.random() * -200000),
				    text: text,
				    createdAt: new Date(),
				    user: {
					__typename: "User",
					...session.activeUser
				    }
				}
			    }
			}).then(({
			    data
			}) => {

			}).catch(e => console.log(e.message));
		    }
		    
		}

	    }>
                <input
	    className="add-snap__input"
	    type = "text"
	    name = "text"
	    value={text}
	    onChange={OnChange}
	    disabled={!(session && session.activeUser)}
	    placeholder={session && session.activeUser ? 'add snap' : 'please login for add a new snap!'} />
		</form>
	    </div>
    )
	


}

class NewSnapForm extends Component {

    render(){

	return(
		<div>
		<Mutation session={
		    this.props.session
		}/>
	    </div>
	)
    }
}

export default NewSnapForm;
