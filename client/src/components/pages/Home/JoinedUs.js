
import React from 'react';

import { useSubscription} from '@apollo/client';
import { USER_CREATED} from '../../../queries';


const Subscription = (props) => {

    const {loading, data} = useSubscription(USER_CREATED);
    return (
	    < div className="joinedUs">
	    {

		!loading &&
		    < div >
		    <strong>{
			data.user.username
		    }</strong> is joined to us.
		    </div>
	    }
	    </div>
    )

}

const JoinedUs = ()=>{
    return(
	    < div >
	    <Subscription/>
	    </div>

    );
}


export default JoinedUs;
