import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_SNAPS,SNAP_CREATED} from '../../../queries';
import SnapListWrapper from './SnapListWrapper';



const Query = ({session}) => {
    const {loading, error, data,subscribeToMore} = useQuery(GET_SNAPS);
    if (loading) return <div className='loading'>Loading snap... </div>;
    if (error) return <div>Error </div>;

    return(
	    <SnapListWrapper subscribeToNewSnaps={()=>{
		subscribeToMore({
		    document: SNAP_CREATED,
		    updateQuery: (prev, { subscriptionData }) => {
			if (!subscriptionData.data) return prev;
			const newItem = subscriptionData.data.snap;
			if(session.activeUser.id !== newItem.user.id){
			    if (!prev.snaps.find(snap => snap.id === newItem.id)) {
				return {
				    ...prev,
				    snaps: [newItem, ...prev.snaps]
				}
			    }else{
				return prev;
			    }
			}
		    }
		});
	    }}
	data={data}
	    />
    );
};




const SnapList = ({session}) => {

    return(
	    <div>
	    <Query session={session} />
	    </div>
    )
}


export default SnapList;
