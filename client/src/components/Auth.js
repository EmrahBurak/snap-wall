import React from 'react';
import { useQuery } from '@apollo/client';
import {GET_ACTIVE_USER} from '../queries';
import {Redirect} from 'react-router-dom';


const Query = async (props) =>{
    const { loading, data } = await useQuery(GET_ACTIVE_USER);
    const Component = props.component;
    const condition = props.condition;
    if (loading) return <p style={{textAlign: 'center'}}>Loading...</p>;

    return condition(data) ? <Component {...props.props}/> :
    <Redirect to="/" />
}

const Auth = (condition) => (component) => props => (
	<div>
	<Query condition={condition}
    component={component}
    props={props}/>
    </div>

);


export default Auth;
