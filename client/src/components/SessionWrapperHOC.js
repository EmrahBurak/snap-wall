import React from 'react';
import { useQuery } from '@apollo/client';
import {GET_ACTIVE_USER} from '../queries';


const Query = (props) => {
    const { loading, data, refetch } = useQuery(GET_ACTIVE_USER);
    const Component = props.component;
    if (loading) return <p style={{textAlign: 'center'}}>Loading...</p>;

    console.log(data);
    return <Component {...props.props} refetch={refetch} session={data}/>;
}


const SessionWrapperHOC = (component) => props =>(
	    < div >
	    <Query component={
		component
	    } props={
		props
	    }/>
	</div>
);


export default SessionWrapperHOC;


